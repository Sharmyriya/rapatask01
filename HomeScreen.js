import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      complete: false,
      pending: false,
    };
  }

  componentDidMount = () => {
    fetch('https://jsonplaceholder.typicode.com/todos', {
      method: 'GET'
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //  console.log(responseJson);
        this.setState({
          data: responseJson
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    return (
      <View style={{ flex: 1, alignSelf: 'center' }}>

        <Text style={{ padding: 20, fontSize: 20 }}>Status</Text>
     {/* button */}
        {this.state.pending == true ? null :
          <Text
            style={{ padding: 20, backgroundColor: 'green' }}
            onPress={() => this.setState({ complete: !this.state.complete })}>
            Completed
            </Text>}

          {/* button */}
        {this.state.complete == true ? null :
          <Text
            style={{ padding: 20, backgroundColor: 'orange', marginTop: 20 }}
            onPress={() => this.setState({ pending: !this.state.pending })}>
            Pending
            </Text>}

        {/* showing completed data */}
        {this.state.complete == true ?
          <View >
            {this.state.data.map((item, index) => {
              return (item.completed == true ?
                <View  key={index}>
                  <Text style={styles.text}>
                    userdId: {item.userId}
                  </Text >
                  <Text style={styles.text}>Title   :{item.title}</Text>
                  <Text style={styles.text}>completed   :{`${item.completed}`}</Text>
                  <View style={{ width: '100%', height: 1, backgroundColor: '#000' }} />
                </View> : null)
            })
            }
          </View>
          : null}
              {/* showing pending data */}
        {this.state.pending == true ?
          <View >
            {this.state.data.map((item, index) => {
              return item.completed == false ?
                <View style={{ flex: 1 }}>
                  <Text style={styles.text}>
                    userdId   :{item.userId}
                  </Text>
                  <Text style={styles.text}>Title    :{item.title}</Text>
                  <Text style={styles.text}>completed   :{`${item.completed}`}</Text>

                  <View style={{ width: '100%', height: 1, backgroundColor: '#000' }} />

                </View> : null
            })
            }
          </View> 
          : null}

      </View>
    );
  }
}

export default HomeScreen;
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    alignSelf: 'center'
  },
  text: {
    fontSize: 15,
    color: '#000',
    textAlign: 'left',
    padding: 7
  }
});
