import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import HomeScreen from './screens/HomeScreen';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      task1: false,
      task2: false,
      standardsList: [
        { "Grade": "Math K", "Domain": "Counting & Cardinality" },
        { "Grade": "Math K", "Domain": "Counting & Cardinality" },
        { "Grade": "Math K", "Domain": "Counting & Cardinality" },
        { "Grade": "Math K", "Domain": "Counting & Cardinality" },
        { "Grade": "Math K", "Domain": "Geometry" },
        { "Grade": "Math 1", "Domain": "Counting & Cardinality" },
        { "Grade": "Math 1", "Domain": "Counting & Cardinality" },
        { "Grade": "Math 1", "Domain": "Orders of Operation" },
        { "Grade": "Math 2", "Domain": "Geometry" },
        { "Grade": "Math 2", "Domain": "Geometry" }
      ],
    }
  }

  render() {
    const newArray = [];
    this.state.standardsList.forEach(obj => {
      if (!newArray.some(o => o.Grade === obj.Grade ||
        o.Domain === obj.Domain)) {
        newArray.push({ ...obj })
      }
    });
    console.log(newArray);
    return (
      <View style={styles.MainContainer}>

        {/* {showing removed duplicate data} */}
        {this.state.task1 == true ?
          <View >
              {newArray.map((item, key) => (

                  <View key={key}>
                    <Text style={styles.text} > Grade = {item.Grade} </Text>
                    <Text style={styles.text} > Domain = {item.Domain} </Text>
                    <View style={{ width: '100%', height: 1, backgroundColor: '#000' }} />
                  </View>
                ))
              }
          </View> 
          : null}

        {/* buttons */}
        <View style={{ justifyContent: 'center', alignSelf: 'center', marginTop: 30 }}>
          {this.state.task2 == true ? null :
            <Text
              style={{ padding: 30, color: 'blue', fontSize: 19, }}
              onPress={() => this.setState({ task1: !this.state.task1 })} >
              Task1
          </Text>}

          {this.state.task1 == true ? null :
            <Text
              style={{ padding: 30, color: 'blue', fontSize: 19, }}
              onPress={() => this.setState({ task2: !this.state.task2 })}>
              Task2
          </Text>}
        </View>

        {/* showing task2 complete and pending status data Homescreeen passed  */}
        {this.state.task2 == true ?
          <View style={{ flex: 1 }} >
            <ScrollView>
              <HomeScreen />
            </ScrollView>
          </View> : null}

      </View>
    );
  }
}

export default App;
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    alignSelf: 'center'
  },
  text: {
    fontSize: 15,
    color: '#000',
    textAlign: 'left',
    padding: 7
  }
});
